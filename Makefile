.DEFAULT_GOAL :=  build
BUILD_DIR=./build
SUBJECTS_DIR=./subjects
TEMPLATE_DIR=./template

build: init
	pdflatex --shell-escape -pdf index.tex -output-directory "${BUILD_DIR}"

init: clean
	mkdir "${BUILD_DIR}"

clean:
	rm -rf "${BUILD_DIR}"
	for FILE in ./index.*; do \
		if [ "$$FILE" != "./index.tex" ]; then \
			rm "$$FILE"; \
		fi; \
	done